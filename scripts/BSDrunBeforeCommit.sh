#!/bin/bash

#============== TRIM HTML ==============

dir="markdown/"

dest="pages/"

media="media/"

if [[ "$(uname)" == "Darwin" ]]; then
    xsed="gsed"
else
    xsed="sed"
fi

shopt -s nullglob
md_files=("$dir"/*.md)

if [ ${#md_files[@]} -gt 0 ]; then

	for filepath in "$dir"/*.md
	do
	    filename=$(basename "$filepath" .md)

		#Move images to correct path
		grep -oP '!\[.*?\]\((?!\.\./)[^)]+\)' "$filepath" | while read -r image_path; do
			image_path=$(echo "$image_path" | sed -E 's/^!\[.*\]\(//; s/\)$//')
			image_name=$(basename "$image_path")
			new_image_path="$media$image_name"
			new_markdown_path="../media/$image_name"

			if [[ "$image_path" == "$new_image_path" ]]; then
				continue
			fi

			# Renmae
			if [[ -f "$new_image_path" ]]; then
				base="${image_name%.*}"  
				ext="${image_name##*.}"  
				counter=1
				while [[ -f "$media/${base}_$counter.$ext" ]]; do
					((counter++))
				done
				new_image_path="$media/${base}_$counter.$ext"
				new_markdown_path="../media/${base}_$counter.$ext"
				echo "Fant duplikat, lagrer som $new_image_path"
			fi

			if [[ -f "$image_path" ]]; then
				cp "$image_path" "$new_image_path"
				echo "Kopierte $image_path -> $new_image_path"
			else
				echo "Advarsel: Bildet $image_path finnes ikke!"
			fi

			# Fix new path
			$xsed -i "s|!\\[.*\\](\\($image_path\\))|![]($new_markdown_path)|g" "$filepath"
		done


	    pandoc -f markdown+emoji $filepath > $dir/$filename.html
	    $xsed -E -i 's/<pre\sclass="([^"]*)">/<pre class="language-\1">/g' $dir/$filename.html
	    
	    # Replace underscores with spaces
	    title=${filename//_/ }

	    #content=$(gtail -c +110 "$filepath" | ghead -c -14)
	    content=$(cat $dir/$filename.html)


	    # Create the new HTML structure with the extracted content and title
	    new_content="<!DOCTYPE html>
	<html lang=\"en\">
	<head>
	    <meta charset=\"UTF-8\">
	    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
	    <link rel="icon" type="image/svg+xml" href="../favicon.svg">
	    <link rel=stylesheet href=style.css>
	    <link id=\"codeStyle\" rel=stylesheet href=prism.css>
	    <script type=module src=script.js></script>
	    <script type=module src=prism.js></script>
	    <title>$title</title>
	</head>
	<body>
	    <div id=\"canvas\">
	        <div id=\"overview\"></div>
	        <div id=\"content\">
	            <h1>$title</h1>
	            $content
	        </div>
	    </div>
	</body>
	</html>"

	    # Write the new content to a file in the destination directory
	    echo "$new_content" > "$dest/$filename.html"

	    # Remove the original file
	    rm "$dir/$filename.html"
	done
else
	echo "Fant ingen markdown filer i markdown mappen"
fi
#============ POPULATE MENU ============
dir="pages/"

# Start of JSON
echo '{ "files": [' > pagelist.json

# Find HTML files, exclude index.html, sort them, and then format as JSON
find $dir -name "*.html" ! -name "index.html" | sort -V | awk -v ORS=', ' '{ sub(".*/",""); print "\""$0"\""}' >> pagelist.json

# End of JSON
$xsed -i '$ s/..$/] }/' pagelist.json
