#!/bin/python3
import tkinter as tk
from tkinter import filedialog, ttk, messagebox
import configparser, os, subprocess, shutil, json, sys

confParse = configparser.ConfigParser()
confFile = "webink.cfg"

if not  os.path.isfile(confFile):
    print("No config found...")
    confParse.add_section('Places')
    confParse.set('Places','prev_project',"")
    with open(confFile, "w") as cfg:
        confParse.write(cfg)
    print("New config created.")

try:
    confParse.read(confFile)
except FileNotFoundError:
    print(confFile+" missing.")
    exit()

def getWebink(webink_path):
   
    section_name = os.path.splitext(os.path.basename(webink_path))[0] 
    section_description = f" "  
    
    if os.path.exists(webink_path):
        config = configparser.ConfigParser()
        config.read(webink_path, encoding="utf-8")

        if 'Info' in config:
            section_name = config['Info'].get('name', section_name)
            section_description = config['Info'].get('description', section_description)

    return section_name, section_description


def enableNewBook(name, index, mode):
    string = projName.get()
    if string != "":
        newButton.config(state="normal")
    else:
        newButton.config(state="disabled")


def enableNewCollection(name, index, mode):
    string = collectionName.get()
    if string != "":
        collectionButton.config(state="normal")
    else:
        collectionButton.config(state="disabled")

def newBook():
    path = filedialog.askdirectory()

    if path == "":
        return
    else:
        name = projName.get()
        path+="/"+name
        os.mkdir(path)
        os.mkdir(path+"/pages")
        shutil.copytree(os.path.dirname(__file__)+"/resources",path,dirs_exist_ok=True)
        os.mkdir(path+"/media")
        os.mkdir(path+"/markdown")
        os.mkdir(path+"/files")
        global projDir
        projDir = path
        setPrevProject(path)
        projectConf.clear()
        projectConf.add_section('Info')
        projectConf.set('Info','name',name)
        projectConf.set('Info','description',"")
        with open(path+"/"+name+".webink","w") as proj:
            projectConf.write(proj)
        drawWindow()

def newCollection():
    global collectionName
    path = filedialog.askdirectory()

    if path == "":
        return
    else:
        name = collectionName.get()
        path+="/"+name
        os.mkdir(path)
        with open(os.path.join(path,"collection.conf"), "w") as configFile:
            configFile.write(f'collection name = "{name}"\n')
        shutil.copy(os.path.join(os.path.dirname(__file__),".", "collection_style.css" ),path)
        shutil.copy(os.path.join(os.path.dirname(__file__),"resources", "logo.svg" ),path)
        shutil.copy(os.path.join(os.path.dirname(__file__),"resources", "favicon.svg" ),path)
        collectionName.set("")
            
def loadBook():
    global projDir
    home_directory = os.path.expanduser("~")
    projDir = filedialog.askopenfilename(filetypes=[("Web Ink prosjekter", "*.webink")], initialdir=home_directory)

    if projDir != "":
        projDir = os.path.dirname(projDir)
        setPrevProject(projDir)
        drawWindow()


def setProjName(path):
    global projName
    webink_path = os.path.join(path, f"{os.path.basename(path)}.webink")  

    if os.path.exists(webink_path):
        projName = getWebink(webink_path)[0]
    else:
        print(f"Error: {webink_path} does not exist.")


def setPrevProject(path):
    confParse.set('Places','prev_project',path)
    with open(confFile, "w") as cfg:
        confParse.write(cfg)

def renameBook(nameEntry):
    global projDir, projectConf
    new_name = nameEntry.get()
    if new_name == "":
        return

    dir_name = projDir.split("/")[-1]
    webink_file = os.path.join(projDir, f"{dir_name}.webink")

    projectConf.read(webink_file)
    projectConf.set('Info', 'name', new_name)
    
    with open(webink_file, "w") as proj:
        projectConf.write(proj)
    
    projectConf.clear()
    projectConf.read(webink_file)
    setProjName(projDir)
    
    drawWindow()

def updateDescription(textWidget):
    global projDir, projectConf
    new_description = textWidget.get("1.0", tk.END).strip()  
    
    dir_name = projDir.split("/")[-1]
    webink_file = os.path.join(projDir, f"{dir_name}.webink")
    
    projectConf.read(webink_file)
    projectConf.set('Info', 'description', new_description)
    
    with open(webink_file, "w") as proj:
        projectConf.write(proj)
    
    projectConf.clear()
    projectConf.read(webink_file)
    
    drawWindow()

def drawWindow():
    global projName, projDir
    def get_directory(path):
        if os.path.isdir(path):
            return path
        else:
            return os.path.dirname(path)

    for widget in window.winfo_children():
        widget.grid_forget()

    if projDir == "":
        global collectionName
        projName = tk.StringVar()
        collectionName = tk.StringVar()
        projNameEntry = tk.Entry(window, textvariable=projName)
        collectionNameEntry = tk.Entry(window, textvariable=collectionName)
        projName.trace_add("write", enableNewBook)
        collectionName.trace_add("write", enableNewCollection)

        tk.Button(window, text="Last inn bok", command=lambda: loadBook()).grid(row=0, column=2)
        tk.Label(window, text="Ny bok:").grid(row=1, column=0)
        projNameEntry.grid(row=1, column=2)
        newButton.grid(row=1, column=3, sticky="w")
        tk.Label(window, text="Ny samling:").grid(row=2, column=0)
        collectionNameEntry.grid(row=2, column=2)
        collectionButton.grid(row=2, column=3)
    else:
        setProjName(projDir)
        projNameNew = tk.StringVar(value=projName)
        projNameEdit = tk.Entry(window, textvariable=projNameNew, width=40)

        dir_name = projDir.split("/")[-1]
        webink_file = os.path.join(projDir, f"{dir_name}.webink")
        print(f"{dir_name}.webink")
        current_description = getWebink(webink_file)[1] 

        tk.Label(window, text=projName).grid(row=0, column=1)
        buildBtn.grid(row=8, column=2)
        projNameEdit.grid(row=2, column=1)
        tk.Button(window, text="Sett nytt navn", command=lambda: renameBook(projNameEdit)).grid(row=2, column=2, sticky="w")

        tk.Label(window, text="Beskrivelse:").grid(row=4, column=0)
        
        descriptionText = tk.Text(window, height=4, width=40)
        descriptionText.insert("1.0", current_description) 
        descriptionText.grid(row=4, column=1, pady=10)
        
        tk.Button(window, text="Oppdater beskrivelse", command=lambda: updateDescription(descriptionText)).grid(row=4, column=2)

        tk.Label(window, text=get_directory(projDir), fg="#666").grid(row=5, column=0, columnspan=3)
        folderBtn.grid(row=6, column=1)
        tk.Label(window, text=" ").grid(row=7, column=0)
        tk.Label(window, text=" ").grid(row=9, column=0)
        closeBtn.grid(row=8, column=0)



def closeBook():
    global projDir
    global projName
    setPrevProject("")
    projDir = ""
    drawWindow()
    projName.set("")


def update_index_html():
    global projDir
    main_folder = os.path.dirname(projDir)
    project_file = os.path.join(main_folder, 'collection.conf')

    if not os.path.exists(project_file):
        return 

    with open(project_file, 'r') as f:
        for line in f:
            if line.startswith('collection name ='):
                project_name = line.split('=')[1].strip().strip('"')
                break
        else:
            project_name = "Collection"
    
    index_path = os.path.join(main_folder, 'index.html')
    subfolders = [f for f in os.listdir(main_folder) if os.path.isdir(os.path.join(main_folder, f))]
    links = []
    
    for subfolder in subfolders:
        webink_path = os.path.join(main_folder, subfolder, f'{subfolder}.webink')
        section_name, section_description = getWebink(webink_path)

        
        pagelist_path = os.path.join(main_folder, subfolder, 'pagelist.json')
        if os.path.exists(pagelist_path):
            with open(pagelist_path, 'r') as f:
                pagelist = json.load(f)
            
            if 'files' in pagelist and isinstance(pagelist['files'], list):
                first_page = pagelist['files'][0]
                page_link = f'''
                <li>
                    <img src="{subfolder}/logo.svg" onerror="this.onerror=null; this.src='{subfolder}/logo.webp'; this.onerror=function(){{ this.src='{subfolder}/logo.png'; this.onerror=function(){{ this.src='{subfolder}/logo.jpg'; }} }}"/>
                    <a href="{subfolder}/pages/{first_page}">{section_name}</a>
                    <p>{section_description}</p>
                </li>
                '''
                links.append(page_link)


    
    index_content = f"""
<!DOCTYPE html>
<html>
<head>
    <title>{project_name}</title>
    <link rel="icon" type="image/svg+xml" href="favicon.svg">
    <link rel=stylesheet href=collection_style.css>
</head>
<body>
    <img src="logo.svg" onerror="this.onerror=null; this.src='logo.webp'; this.onerror=function(){{ this.src='logo.png'; this.onerror=function(){{ this.src='logo.jpg'; }} }}"/>
    <h1>{project_name}</h1>
    <ul>
{chr(10).join(links)}
    </ul>
</body>
</html>
"""
    
    with open(index_path, 'w') as index_file:
        index_file.write(index_content)


def buildWebsite():
    global projDir
    subprocess.run(["bash", os.path.dirname(__file__) + "/scripts/BSDrunBeforeCommit.sh"], cwd=projDir, check=True)
    update_index_html()

def openWorkFolder():
    if sys.platform == "darwin":  # macOS
        subprocess.call(['open', projDir])
    elif sys.platform == "linux" or sys.platform == "linux2":  # Linux
        subprocess.call(['xdg-open', projDir])
    elif sys.platform == "win32":  # Windows
        subprocess.call(['explorer', projDir])


projDir = confParse.get("Places","prev_project")
if not os.path.isdir(projDir):
    print(f"Fant ikke bok: {projDir}")
    projDir = "" 
    setPrevProject(projDir)

window = tk.Tk()
window.title("WebInk")


projectConf = configparser.ConfigParser()
newButton = tk.Button(window,text="opprett ny bok", command=lambda : newBook(),state="disabled")
collectionButton = tk.Button(window,text="opprett ny samling", command=lambda : newCollection(),state="disabled")
#liveBuildBtn = tk.Button(window, text="Slå på live build", state="disabled")
buildBtn = tk.Button(window, text="Bygg nettside", bg="lightgreen", activebackground="lime",command=lambda : buildWebsite())
folderBtn = tk.Button(window, text="Åpne arbeidsmappe", command=lambda : openWorkFolder())
closeBtn = tk.Button(window, text="Lukk bok", bg="salmon", activebackground="red", command=lambda : closeBook())

drawWindow()

window.mainloop()
